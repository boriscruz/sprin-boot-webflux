package com.moggix.springboot.reactor.webflux.app;

import java.math.BigDecimal;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;

import com.moggix.springboot.reactor.webflux.app.models.dao.GoodsDAO;
import com.moggix.springboot.reactor.webflux.app.models.documents.Goods;

import reactor.core.publisher.Flux;

@SpringBootApplication
public class SpringBootWebfluxApplication implements CommandLineRunner {

	private static final Logger log = LoggerFactory.getLogger(SpringBootWebfluxApplication.class);
	
	@Autowired
	private GoodsDAO goodsDAO;
	
	@Autowired
	private ReactiveMongoTemplate mongoTemplate;
	
	public static void main(String[] args) {
		SpringApplication.run(SpringBootWebfluxApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		mongoTemplate.dropCollection("goods");
		
		Flux.just(
				new Goods("Tv panasonic screen LCD", BigDecimal.valueOf(15500.22)),
				new Goods("Sony camara HD digital", BigDecimal.valueOf(256.22)),
				new Goods("Notebook", BigDecimal.valueOf(457.22)),
				new Goods("Cellphone Xiaomi", BigDecimal.valueOf(5872.22)),
				new Goods("Printer Epson 4200", BigDecimal.valueOf(457.22)),
				new Goods("Apple Ipod", BigDecimal.valueOf(4567.22)),
				new Goods("TV sony", BigDecimal.valueOf(985.22))
				)
		.flatMap(goods -> {
			goods.setCreateAt(new Date());
			return goodsDAO.save(goods);
			})
		.subscribe(System.out::println);
		
	}

}
