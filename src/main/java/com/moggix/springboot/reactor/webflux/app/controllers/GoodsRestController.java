package com.moggix.springboot.reactor.webflux.app.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.moggix.springboot.reactor.webflux.app.models.dao.GoodsDAO;
import com.moggix.springboot.reactor.webflux.app.models.documents.Goods;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/goods")
public class GoodsRestController {

	public static final Logger log = LoggerFactory.getLogger(GoodsController.class);

	@Autowired
	private GoodsDAO goodsDAO;

	@GetMapping
	public Flux<Goods> index() {

		Flux<Goods> goodsList = goodsDAO.findAll().map(good -> {
			good.setDenomination(good.getDenomination().toUpperCase());
			return good;
		}).doOnNext(g -> log.info(g.toString()));
		return goodsList;
	}

	@GetMapping("/{id}")
	public Mono<Goods> show(@PathVariable String id) {

		Flux<Goods> goodsList = goodsDAO.findAll();

		Mono<Goods> good = goodsList.filter(g -> g.getId().equals(id)).next().doOnNext(g -> log.info(g.toString()));

		return good;
	}

}
