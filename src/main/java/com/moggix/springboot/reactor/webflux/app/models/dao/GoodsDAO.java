package com.moggix.springboot.reactor.webflux.app.models.dao;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import com.moggix.springboot.reactor.webflux.app.models.documents.Goods;

public interface GoodsDAO extends ReactiveMongoRepository<Goods, String> {

}
