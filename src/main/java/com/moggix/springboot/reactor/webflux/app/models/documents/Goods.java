package com.moggix.springboot.reactor.webflux.app.models.documents;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "goods")
public class Goods {

	@Id
	private String id;

	private String denomination;

	private BigDecimal price;

	private Date createAt;

	public Goods(String denomination, BigDecimal price) {
		this.denomination = denomination;
		this.price = price;
	}

	public Goods() {
	}

}
