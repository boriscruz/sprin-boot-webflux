package com.moggix.springboot.reactor.webflux.app.controllers;

import java.time.Duration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.thymeleaf.spring5.context.webflux.ReactiveDataDriverContextVariable;

import com.moggix.springboot.reactor.webflux.app.models.dao.GoodsDAO;
import com.moggix.springboot.reactor.webflux.app.models.documents.Goods;

import reactor.core.publisher.Flux;

@Controller
public class GoodsController {

	public static final Logger log = LoggerFactory.getLogger(GoodsController.class);

	@Autowired
	private GoodsDAO goodsDAO;

	@GetMapping({ "/toList", "/" })
	public String toList(Model model) {

		Flux<Goods> goodsList = goodsDAO.findAll().map(goods -> {
			goods.setDenomination(goods.getDenomination().toUpperCase());
			return goods;
		});

		goodsList.subscribe(good -> log.info(good.toString()));

		model.addAttribute("goods", goodsList);
		model.addAttribute("title", "Goods List");

		return "toList";
	}

	@GetMapping({ "/toList-datadriver" })
	public String toListDataDriver(Model model) {

		Flux<Goods> goodsList = goodsDAO.findAll().map(goods -> {
			goods.setDenomination(goods.getDenomination().toUpperCase());
			return goods;
		}).delayElements(Duration.ofSeconds(1));

		goodsList.subscribe(good -> log.info(good.toString()));

		model.addAttribute("goods", new ReactiveDataDriverContextVariable(goodsList, 1));
		model.addAttribute("title", "Goods List");

		return "toList";
	}
}
